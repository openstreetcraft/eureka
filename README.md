[![pipeline status](https://gitlab.com/openstreetcraft/eureka/badges/master/pipeline.svg)](https://gitlab.com/openstreetcraft/eureka/commits/master)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/2ca2fc01ba2b4969830c729622a16a28)](https://www.codacy.com/gl/openstreetcraft/eureka/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=openstreetcraft/eureka&amp;utm_campaign=Badge_Grade)
[![Javadoc Badge](https://img.shields.io/badge/javadoc-brightgreen.svg)](https://openstreetcraft.gitlab.io/eureka/docs/javadoc/)

This application provides the [Eureka Server](https://github.com/spring-cloud-samples/eureka) that provides service discovery and enables all Eureka clients to discover each other.

When a client registers with Eureka, it provides meta-data about itself such as host and port, health indicator URL, home page etc.
Eureka receives heartbeat messages from each instance belonging to a service.
If the heartbeat fails over a configurable timetable, the instance is normally removed from the registry.

A Docker image is build with the [Gradle Docker Plugin](https://github.com/Transmode/gradle-docker).
This Docker image is published to GitLab Docker registry and deployed as [AWS ECS instance](https://aws.amazon.com/de/documentation/ecs/).

# Deployment

## Development environment

```
./gradlew distDocker
docker-compose up --build eureka
```

## Production environment

```
CI_BUILD_REF=master docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d eureka
```
